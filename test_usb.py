from pprint import pprint
import usb
import usb.util

for i, dev in enumerate(usb.core.find(find_all=True)):
    #print(i, dev)
    interface = dev.get_active_configuration().interfaces()[0]
    dev_name = dev.product
    dev_type = usb.core._try_lookup(usb.core._lu.interface_classes, interface.bInterfaceClass)
    dev_vendor = dev.manufacturer
    dev_id = dev.idVendor << 16 | dev.idProduct
    print(i, hex(dev_id), dev_name, dev_type, dev_vendor, sep=',\t')
    # print(i, usb.core._try_lookup(usb.core._lu.device_classes, dev.bDeviceClass), dev.bDeviceClass)
    # print(i, usb.util.get_string(dev, dev.bDeviceClass))

