module HolaMundo
{
    struct Device
    {
        string name;
        string type;
        string vendor;
    }

    dictionary<int, Device> ConnectedDevices;

    interface DeviceInfo
    {
        ConnectedDevices getConnectedDevices();
        Device getDeviceInfo(int id);
    }
}
