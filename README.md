# Hola mundo (extendido) - Modelamiento de sistemas EMI 210 M3 - 2023/2

En este repositorio se aloja el proyecto final para el módulo 3 de la asignatura Modelamiento de sistemas EMI 210.

## Resumen del proyecto

El proyecto consiste en un sistema distribuido de dos hosts e donde se intercambia información entre ambos en la modalidad cliente-servidor. En la siguiente figura, se muestra un esquema general del funcionamiento del sistema.

![esquema general](esquema-general.png)

El servidor consta del sistema operativo Ubuntu server y la aplicación que provee los servicios está implementada en Java y corre en el puerto 10100 del servidor.
Por políticas de seguridad, solo es posible acceder a este puerto interno mediante un túnel SSH entre el cliente y el servidor.

El cliente cuenta con Windows 10 como sistema operativo y la aplicacion que se ejecuta como cliente está escrita en Python.

El servidor tiene conexión con múltiples dispositivos de hardware tales como teclados, impresoras, punteros, cámaras, entre otros. Estos dispositivos, dependiendo del día, pueden estar conectados o no, por lo que se proveen dos funciones que el cliente puede ejecutar:

* `dictionary getConnectedDevices()`: Al llamar esta función se obtiene un diccionario con el id de dispositivo como llave y una estructura `device` representando el dispositivo como valor.

* `struct getDeviceInfo(id)`: Si se conoce el id del dispositivo, se puede acceder a la información de éste directamente.

La estructura `device` tiene información como el nombre, el tipo, y la marca del dispositivo.
