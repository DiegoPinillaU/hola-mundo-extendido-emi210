import java.util.Map;
import java.util.Scanner;
import HolaMundo.Device;

public class ClientJava
{
    public static void main(String[] args)
    {
        try(com.zeroc.Ice.Communicator communicator = com.zeroc.Ice.Util.initialize(args))
        {
            com.zeroc.Ice.ObjectPrx base = communicator.stringToProxy("DeviceInfo:default -p 10100");
            HolaMundo.DeviceInfoPrx deviceInfo = HolaMundo.DeviceInfoPrx.checkedCast(base);
            if(deviceInfo == null)
            {
                throw new Error("Invalid proxy");
            }

            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.println("Ingresa una opción");
                System.out.println("[1] Mostrar información de todos los dispositivos");
                System.out.println("[2] Ver información de un dispositivo");
    
                int option = scanner.nextInt();
    
                switch (option) {
                    case 1:
                        deviceInfo.getConnectedDevices().forEach(
                            (id, dev) -> {
                                System.out.printf("Dispositivo %d\n\tNombre: %s\n\tTipo: %s\n\tFabricante: %s\n", id, dev.name, dev.type, dev.vendor);
                            }
                        );
                        break;
                
                    case 2:
                        System.out.println("Ingrese el id del dispositivo");
                        int devId = scanner.nextInt();
                        HolaMundo.Device dev = deviceInfo.getDeviceInfo(devId);
                        System.out.printf("Dispositivo %d\n\tNombre: %s\n\tTipo: %s\n\tFabricante: %s\n", devId, dev.name, dev.type, dev.vendor);
                    default:
                        break;
                }
            }

        }
    }
}