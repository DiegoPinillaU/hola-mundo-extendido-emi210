import sys, Ice
import HolaMundo
import signal
import usb

class DeviceInfoI(HolaMundo.DeviceInfo):
    def getConnectedDevices(self, current=None):
        connectedDevices = {}
        for i, dev in enumerate(usb.core.find(find_all=True)):
            interface = dev.get_active_configuration().interfaces()[0]
            dev_name = dev.product
            dev_type = usb.core._try_lookup(usb.core._lu.interface_classes, interface.bInterfaceClass)
            dev_vendor = dev.manufacturer
            dev_id = dev.idVendor << 16 | dev.idProduct
            connectedDevices[dev_id] = HolaMundo.Device(
                name=dev_name,
                type=dev_type,
                vendor=dev_vendor
            )
        return connectedDevices
    
    def getDeviceInfo(self, id, current=None):
        idVendor = id >> 16
        idProduct = id & 0xFFFF
        dev = usb.core.find(idProduct=idProduct, idVendor=idVendor)
        print(dev)
        if dev:
            interface = dev.get_active_configuration().interfaces()[0]
            dev_name = dev.product
            dev_type = usb.core._try_lookup(usb.core._lu.interface_classes, interface.bInterfaceClass)
            dev_vendor = dev.manufacturer
            return HolaMundo.Device(
                    name=dev_name,
                    type=dev_type,
                    vendor=dev_vendor
                )
 
with Ice.initialize(sys.argv) as communicator:
    print('Iniciando adaptador en localhost:10100, pulse Ctrl-C para finalizar')

    #
    # Install a signal handler to shutdown the communicator on Ctrl-C
    #
    signal.signal(signal.SIGINT, lambda signum, frame: communicator.shutdown())
    if hasattr(signal, 'SIGBREAK'):
        signal.signal(signal.SIGBREAK, lambda signum, frame: communicator.shutdown())
    adapter = communicator.createObjectAdapterWithEndpoints("DeviceInfoAdapter", "default -h localhost -p 10100")
    adapter.add(DeviceInfoI(), Ice.stringToIdentity("DeviceInfo"))
    adapter.activate()
    communicator.waitForShutdown()