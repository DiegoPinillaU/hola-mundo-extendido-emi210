# -*- coding: utf-8 -*-
#
# Copyright (c) ZeroC, Inc. All rights reserved.
#
#
# Ice version 3.7.10
#
# <auto-generated>
#
# Generated from file `DeviceInfo.ice'
#
# Warning: do not edit this file.
#
# </auto-generated>
#

from sys import version_info as _version_info_
import Ice, IcePy

# Start of module HolaMundo
_M_HolaMundo = Ice.openModule('HolaMundo')
__name__ = 'HolaMundo'

if 'Device' not in _M_HolaMundo.__dict__:
    _M_HolaMundo.Device = Ice.createTempClass()
    class Device(object):
        def __init__(self, name='', type='', vendor=''):
            self.name = name
            self.type = type
            self.vendor = vendor

        def __hash__(self):
            _h = 0
            _h = 5 * _h + Ice.getHash(self.name)
            _h = 5 * _h + Ice.getHash(self.type)
            _h = 5 * _h + Ice.getHash(self.vendor)
            return _h % 0x7fffffff

        def __compare(self, other):
            if other is None:
                return 1
            elif not isinstance(other, _M_HolaMundo.Device):
                return NotImplemented
            else:
                if self.name is None or other.name is None:
                    if self.name != other.name:
                        return (-1 if self.name is None else 1)
                else:
                    if self.name < other.name:
                        return -1
                    elif self.name > other.name:
                        return 1
                if self.type is None or other.type is None:
                    if self.type != other.type:
                        return (-1 if self.type is None else 1)
                else:
                    if self.type < other.type:
                        return -1
                    elif self.type > other.type:
                        return 1
                if self.vendor is None or other.vendor is None:
                    if self.vendor != other.vendor:
                        return (-1 if self.vendor is None else 1)
                else:
                    if self.vendor < other.vendor:
                        return -1
                    elif self.vendor > other.vendor:
                        return 1
                return 0

        def __lt__(self, other):
            r = self.__compare(other)
            if r is NotImplemented:
                return r
            else:
                return r < 0

        def __le__(self, other):
            r = self.__compare(other)
            if r is NotImplemented:
                return r
            else:
                return r <= 0

        def __gt__(self, other):
            r = self.__compare(other)
            if r is NotImplemented:
                return r
            else:
                return r > 0

        def __ge__(self, other):
            r = self.__compare(other)
            if r is NotImplemented:
                return r
            else:
                return r >= 0

        def __eq__(self, other):
            r = self.__compare(other)
            if r is NotImplemented:
                return r
            else:
                return r == 0

        def __ne__(self, other):
            r = self.__compare(other)
            if r is NotImplemented:
                return r
            else:
                return r != 0

        def __str__(self):
            return IcePy.stringify(self, _M_HolaMundo._t_Device)

        __repr__ = __str__

    _M_HolaMundo._t_Device = IcePy.defineStruct('::HolaMundo::Device', Device, (), (
        ('name', (), IcePy._t_string),
        ('type', (), IcePy._t_string),
        ('vendor', (), IcePy._t_string)
    ))

    _M_HolaMundo.Device = Device
    del Device

if '_t_ConnectedDevices' not in _M_HolaMundo.__dict__:
    _M_HolaMundo._t_ConnectedDevices = IcePy.defineDictionary('::HolaMundo::ConnectedDevices', (), IcePy._t_int, _M_HolaMundo._t_Device)

_M_HolaMundo._t_DeviceInfo = IcePy.defineValue('::HolaMundo::DeviceInfo', Ice.Value, -1, (), False, True, None, ())

if 'DeviceInfoPrx' not in _M_HolaMundo.__dict__:
    _M_HolaMundo.DeviceInfoPrx = Ice.createTempClass()
    class DeviceInfoPrx(Ice.ObjectPrx):

        def getConnectedDevices(self, context=None):
            return _M_HolaMundo.DeviceInfo._op_getConnectedDevices.invoke(self, ((), context))

        def getConnectedDevicesAsync(self, context=None):
            return _M_HolaMundo.DeviceInfo._op_getConnectedDevices.invokeAsync(self, ((), context))

        def begin_getConnectedDevices(self, _response=None, _ex=None, _sent=None, context=None):
            return _M_HolaMundo.DeviceInfo._op_getConnectedDevices.begin(self, ((), _response, _ex, _sent, context))

        def end_getConnectedDevices(self, _r):
            return _M_HolaMundo.DeviceInfo._op_getConnectedDevices.end(self, _r)

        def getDeviceInfo(self, id, context=None):
            return _M_HolaMundo.DeviceInfo._op_getDeviceInfo.invoke(self, ((id, ), context))

        def getDeviceInfoAsync(self, id, context=None):
            return _M_HolaMundo.DeviceInfo._op_getDeviceInfo.invokeAsync(self, ((id, ), context))

        def begin_getDeviceInfo(self, id, _response=None, _ex=None, _sent=None, context=None):
            return _M_HolaMundo.DeviceInfo._op_getDeviceInfo.begin(self, ((id, ), _response, _ex, _sent, context))

        def end_getDeviceInfo(self, _r):
            return _M_HolaMundo.DeviceInfo._op_getDeviceInfo.end(self, _r)

        @staticmethod
        def checkedCast(proxy, facetOrContext=None, context=None):
            return _M_HolaMundo.DeviceInfoPrx.ice_checkedCast(proxy, '::HolaMundo::DeviceInfo', facetOrContext, context)

        @staticmethod
        def uncheckedCast(proxy, facet=None):
            return _M_HolaMundo.DeviceInfoPrx.ice_uncheckedCast(proxy, facet)

        @staticmethod
        def ice_staticId():
            return '::HolaMundo::DeviceInfo'
    _M_HolaMundo._t_DeviceInfoPrx = IcePy.defineProxy('::HolaMundo::DeviceInfo', DeviceInfoPrx)

    _M_HolaMundo.DeviceInfoPrx = DeviceInfoPrx
    del DeviceInfoPrx

    _M_HolaMundo.DeviceInfo = Ice.createTempClass()
    class DeviceInfo(Ice.Object):

        def ice_ids(self, current=None):
            return ('::HolaMundo::DeviceInfo', '::Ice::Object')

        def ice_id(self, current=None):
            return '::HolaMundo::DeviceInfo'

        @staticmethod
        def ice_staticId():
            return '::HolaMundo::DeviceInfo'

        def getConnectedDevices(self, current=None):
            raise NotImplementedError("servant method 'getConnectedDevices' not implemented")

        def getDeviceInfo(self, id, current=None):
            raise NotImplementedError("servant method 'getDeviceInfo' not implemented")

        def __str__(self):
            return IcePy.stringify(self, _M_HolaMundo._t_DeviceInfoDisp)

        __repr__ = __str__

    _M_HolaMundo._t_DeviceInfoDisp = IcePy.defineClass('::HolaMundo::DeviceInfo', DeviceInfo, (), None, ())
    DeviceInfo._ice_type = _M_HolaMundo._t_DeviceInfoDisp

    DeviceInfo._op_getConnectedDevices = IcePy.Operation('getConnectedDevices', Ice.OperationMode.Normal, Ice.OperationMode.Normal, False, None, (), (), (), ((), _M_HolaMundo._t_ConnectedDevices, False, 0), ())
    DeviceInfo._op_getDeviceInfo = IcePy.Operation('getDeviceInfo', Ice.OperationMode.Normal, Ice.OperationMode.Normal, False, None, (), (((), IcePy._t_int, False, 0),), (), ((), _M_HolaMundo._t_Device, False, 0), ())

    _M_HolaMundo.DeviceInfo = DeviceInfo
    del DeviceInfo

# End of module HolaMundo
